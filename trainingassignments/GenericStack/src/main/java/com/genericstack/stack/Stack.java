/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.genericstack.stack;

import java.util.*;

/**
 *
 * @author aakriti
 */
public class Stack<T> {

    int top;//Stores position of each element in Stack
    int size;
    T[] stack;

    public Stack() {
        this.top = -1;
        this.size = 4;
        stack = (T[]) new Object[size];

    }

    public boolean isEmpty() {//checks if stack is empty or not
        return top == -1;
    }

    public boolean isFull() {//checks if stack is full or not
        return top == size - 1;
    }

    public T push(T element) {//pushes elements into stack
        if (isFull()) {
            System.out.println("Stack is full");
        }

        return stack[++top] = element;

    }

    public T pop() {//pops element from stack
        if (isEmpty()) {
            System.out.println("Stack is empty");
        }

        return stack[top--];

    }

}
