import React, { Component } from 'react';
import axios from 'axios'
import Input from './input'
import Button from './button'
import '../App.css'
// import SignUp from './signup';
// import { Link, Route, BrowserRouter as Router, Switch } from 'react-router-dom';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        }
    }
    //on entering the value in input field and setting to state
    changeEvent(event) { 
        // console.log(event.target.name);
        // console.log(event.target.value);
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value });
        // console.log(this.state);
};
//submit event 
    submitEvent(event) {
        //preventing the default value of submit button and feaching the data from data base.
        event.preventDefault();
        console.log(this.state);

        axios.get('http://localhost:9090/user/getuser')
            .then((response) => {
                console.log(response)
            })
            .catch(function (error) {
                console.log(error)
            })
    }
    render() {
        return (<div className="login">
            <form onSubmit={this.submitEvent.bind(this)} className="form">
                <h1>Login</h1>
                <hr />
                <Input Name="username" Type="text" event={this.changeEvent.bind(this)} />
                <Input Name="password" Type="password" event={this.changeEvent.bind(this)} />
                <hr />
                <Button Type="submit" Value="Login" Id="signupBtn" />
            </form>
            {/* <Router>
                <Link to='/signup'><span>click here for signup</span></Link>
                <Switch>
                    <Route component={SignUp} path='/signup' exact strict />
                </Switch> */}
            {/* </Router> */}
        </div>
        );
    }
}

export default Login;