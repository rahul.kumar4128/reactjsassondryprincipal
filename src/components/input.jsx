import React, { Component } from 'react';
class Input extends Component {


    render() {

        return (
            <>
                <label htmlFor={this.props.Name}>{this.props.Name}:-</label><br />
                <input type={this.props.Type} name={this.props.Name} id={this.props.Name} placeholder={`enter your ${this.props.Name}`} onChange={this.props.event} required /><br />
            </>
        );
    }
}

export default Input;